       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRADER.
       AUTHOR. KHUMMEUNG WATTANASAROJ.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT INPUT-FILE ASSIGN TO "trader4.dat"
           ORGANIZATION IS LINE SEQUENTIAL.

           SELECT OUTPUT-FILE ASSIGN TO "trader.rpt"
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  INPUT-FILE.
       01  INPUT-BUFFER.
           88 END-OF-INPUT-FILE VALUE HIGH-VALUE .
           05 COL-A PIC   9(2).
           05 COL-B PIC   9(4).
           05 COL-C PIC   9(6).

       FD  OUTPUT-FILE.
       01  OUTPUT-BUFFER.
           88 END-OF-OUTPUT-FILE VALUE HIGH-VALUE .
           05 OUTPUT-DATA PIC X(99).
         



       WORKING-STORAGE SECTION. 
       01  COL-A-PROCESSING  PIC X(2).
       01  COL-A-TOTAL PIC   9(5).
       01  COL-B-PROCESSING  PIC 9(4).
       01  COL-C-PROCESSING  PIC 9(6).


       01  RPT-HEADER.
           05 FILLER PIC X(8)   VALUE "PROVINCE".
           05 FILLER PIC X(4)   VALUE "    ".
           05 FILLER PIC X(8)   VALUE "P INCOME".
           05 FILLER PIC X(4)   VALUE "    ".
           05 FILLER PIC X(6)   VALUE "MEMBER".
           05 FILLER PIC X(2)   VALUE "  ".
           05 FILLER PIC X(6)   VALUE "MEMBER".
           05 FILLER PIC X      VALUE " ".
           05 FILLER PIC X(6)   VALUE "INCOME".
       
       01  RPT-ROW.
           05 NUM-PRO  PIC BBBB99.
           05 FILLER PIC X(5)   VALUE "     ".
           05 TOTAL-PRO-INCOME  PIC $$$$,$$9,999.
           05 FILLER PIC X(3)   VALUE "   ".
           05 MEMBER-MAX-INCOME-ID  PIC   ZZZZ.
           05 FILLER PIC X(7)   VALUE "       ".
           05 MEMBER-MAX-INCOME PIC $$$9,999.

       01  PRN-FOOTER.
           05 FILLER PIC X(14)  VALUE "MAX PROVINCE: ".
           05 TOTAL-NUM-PRO  PIC   99.
           05 FILLER PIC  X(2)  VALUE "  ".
           05 FILLER PIC  X(11) VALUE "SUM INCOME:".
           05 SUM-INCOME  PIC $$$$,999,999.

       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY RPT-HEADER 
           DISPLAY RPT-ROW 
           DISPLAY PRN-FOOTER 
           OPEN INPUT INPUT-FILE 
           OPEN OUTPUT OUTPUT-FILE 
           PERFORM READ-FILE
           PERFORM DISPLAY-FILE-DETALS 
      *    PERFORM PROCESS-COL-A UNTIL END-OF-INPUT-FILE
           DISPLAY TOTAL-NUM-PRO 
           CLOSE OUTPUT-FILE
           CLOSE INPUT-FILE
       .

      *PROCESS-COL-A.
      *    MOVE COL-A TO COL-A-PROCESSING 
      *    MOVE COL-A-PROCESSING TO NUM-PRO 
      *    MOVE ZERO TO COL-A-TOTAL
      *    PERFORM PROCESS-COL-B UNTIL COL-A NOT = COL-A-PROCESSING
      *    DISPLAY COL-A-TOTAL 
      *.

      *PROCESS-COL-B.
      *    MOVE COL-B TO COL-B-PROCESSING
      *    ADD 1 TO COL-A-TOTAL 
      *    PERFORM PROCESS-COL-C UNTIL COL-C NOT = COL-C-PROCESSING 
      *.
       
      *PROCESS-COL-C.
      *    MOVE COL-C TO COL-C-PROCESSING
      *    ADD COL-C-PROCESSING TO MEMBER-MAX-INCOME 

      *.

       DISPLAY-FILE-DETALS.
           PERFORM UNTIL END-OF-INPUT-FILE 
              MOVE COL-A TO NUM-PRO
              MOVE COL-B TO MEMBER-MAX-INCOME-ID  
              MOVE COL-C TO TOTAL-PRO-INCOME 
      *       DISPLAY INPUT-BUFFER 
              DISPLAY RPT-ROW 
           END-PERFORM 
       .
       
       READ-FILE. 
           READ INPUT-FILE 
           AT END 
              SET END-OF-INPUT-FILE TO TRUE
           END-READ
       .
       
      *WRITE-FILE.
      *    DISPLAY " "
      *.
